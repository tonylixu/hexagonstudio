hexagonstudio
    manage.py
    hexagonstudio/
        __init__.py
        settings.py
        urls.py
        wsgi.py

hexagonstudio/: root directory is the container
manage.py: A command-line utility that lets you interact with Django
Inner hexagonstudio/: The actual Python package
hexagonstudio/__init__.py: Tells Python that this directory should be considered a Python package
hexagonstudio/settings.py: Settings/configuration for hexagonstudio project
hexagonstudio/urls.py: The URL declarations for hexagonstudio project; a "table of contents" of the site
hexagonstudio/wsgi.py: An entry-point for WSGI-compatible web servers.
